COMMENT ON SCHEMA public IS '{"version": 10}';

CREATE DOMAIN user_display_name AS VARCHAR(64);
CREATE DOMAIN username AS VARCHAR(64);
