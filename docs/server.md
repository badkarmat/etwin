# Server

## Environment

| Name     | Version                |
|----------|------------------------|
| JRE      | 14                     |
| Nginx    | 1.18                   |
| Node.js  | Any supported by `nvm` |
| PHP      | 7.4                    |
| Postgres | 13.1                   |
| Ruby     | 2.7                    |

## Network

- Internal name: `siman.eternal-twin.net`
- IPv4: `54.38.241.200`
- IPv6: `2001:41d0:305:2100::5e4`
