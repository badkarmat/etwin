[Home](./index.md)

# Eternal-Twin Documentation

Welcome to the Eternal-Twin documentation. This section describes various technical aspects relating to Eternal-Twin.

## General

- [Desktop Application](./desktop.md)
- [API](./api/index.md)

## [Application development](./app/index.md)

- [Guidelines](./app/guidelines.md)
- [Application Configuration](./app/config.md)
- [Eternal-Twin integration](./app/etwin-integration.md)
- [Eternal-Twin for OAuth](./app/etwin-oauth.md)
- [Eternal-Twin API](./app/etwin-api.md)

## [Tools](./tools/index.md)

- [Apache](./tools/apache.md)
- [Node.js](./tools/node.md)
- [package.json](./tools/package-json.md)
- [Yarn](./tools/yarn.md)

## Other

- [OAuth](./oauth.md)
- [Postgres](./db.md)
- [Server](./server.md)
