[Home](./index.md)

# Desktop Application

Eternal-Twin provides a desktop application. It allows you to play Motion Twin's Flash games.

## Download

Latest version: 0.4.3 (2021-01-09)

- [Windows](https://eternal-twin.net/assets/app/etwin-0.4.3-windows-x64.zip)
- [Linux](https://eternal-twin.net/assets/app/etwin-0.4.3-linux-x64.zip)
- [Mac](https://eternal-twin.net/assets/app/etwin-0.4.3-mac-x64.zip)

## Contribute

[Repository](https://gitlab.com/eternal-twin/etwin-app)

The application is still in an alpha state: please help us to improve it.

In particular, we need to complete [the homepage](https://gitlab.com/eternal-twin/etwin-app/-/blob/master/src/main/index.html) to have
links for all the games.
We also need to complete [the list of websites where Flash is allowed](https://gitlab.com/eternal-twin/etwin-app/-/blob/master/src/main/mms.cfg).

## Issues

Report issues [on Gitlab](https://gitlab.com/eternal-twin/etwin-app/-/issues).
